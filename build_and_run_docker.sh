#!/usr/bin/env bash

# Get current working directory and project directory name
CWD=$(pwd)
PJT_DIRECTORY=${CWD##*/}

# Build the docker image
sudo docker build -t ml_2019 -f Dockerfile.open-set-ll-2019 .

# Run container, mounting the project directory, and place inside docker
sudo docker run --rm -it -v $CWD:/$PJT_DIRECTORY/ ml_2019 bash
