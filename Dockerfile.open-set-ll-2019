# ==================================================================
# tool list
# ------------------------------------------------------------------
# apt-utils         latest (apt)
# build-essential   latest (apt)
# ca-certificates   latest (apt)
# cmake             latest (git)
# git               latest (apt)
# python            3.6    (apt)
# wget              latest (apt)
# vim               latest (apt)
# ==================================================================
# ==================================================================
# python module list
# ------------------------------------------------------------------
# cloudpickle   latest (pip)
# Cython        latest (pip)
# jupyter       latest (pip)
# keras         latest (pip)
# matplotlib    latest (pip)
# numpy         latest (pip)
# onnx          latest (pip)
# opencv        4.0.1  (git)
# pandas        latest (pip)
# pytest        latest (pip)
# pylint        latest (pip)
# pytorch       latest (pip)
# scikit-learn  latest (pip)
# scipy         latest (pip)
# seaborn       latest (pip)
# sonnet        latest (pip)
# tensorflow    latest (pip)
# ==================================================================

FROM ubuntu:18.04
ENV LANG C.UTF-8
ARG APT_INSTALL="apt-get install -y"
ARG PIP_INSTALL="python -m pip --no-cache-dir install --upgrade"
ARG GIT_CLONE="git clone --depth 10"

RUN rm -rf /var/lib/apt/lists/* \
           /etc/apt/sources.list.d/cuda.list \
           /etc/apt/sources.list.d/nvidia-ml.list;

RUN apt-get update && yes | apt-get upgrade;

# ==================================================================
# tools
# ------------------------------------------------------------------

RUN DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        build-essential \
        apt-utils \
        ca-certificates \
        wget \
        git \
        vim;

RUN $GIT_CLONE https://github.com/Kitware/CMake ~/cmake; \
    cd ~/cmake; \
    ./bootstrap; \
    make -j"$(nproc)" install;

# ==================================================================
# python
# ------------------------------------------------------------------

RUN DEBIAN_FRONTEND=noninteractive $APT_INSTALL software-properties-common; \
    add-apt-repository ppa:deadsnakes/ppa; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        python3.6 \
        python3.6-dev \
        python3-distutils-extra; \
    wget -O ~/get-pip.py https://bootstrap.pypa.io/get-pip.py; \
    python3.6 ~/get-pip.py; \
    ln -s /usr/bin/python3.6 /usr/local/bin/python3; \
    ln -s /usr/bin/python3.6 /usr/local/bin/python; \
    $PIP_INSTALL setuptools; \
    $PIP_INSTALL \
        cloudpickle \
        Cython \
        matplotlib \
        numpy \
        pandas \
        pylint \
        pytest \
        seaborn \
        scikit-learn \
        scipy;

# ==================================================================
# jupyter
# ------------------------------------------------------------------

RUN $PIP_INSTALL jupyter;

# ==================================================================
# onnx
# ------------------------------------------------------------------

RUN DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        protobuf-compiler \
        libprotoc-dev; \
    $PIP_INSTALL onnx;

# ==================================================================
# pytorch
# ------------------------------------------------------------------

RUN $PIP_INSTALL \
        future \
        numpy \
        protobuf \
        enum34 \
        pyyaml \
        typing \
    	torch; \
    $PIP_INSTALL "git+https://github.com/pytorch/vision.git"; \
    $PIP_INSTALL \
        torch_nightly -f \
        https://download.pytorch.org/whl/nightly/cpu/torch_nightly.html;

# ==================================================================
# tensorflow
# ------------------------------------------------------------------

RUN $PIP_INSTALL tf-nightly-2.0-preview;

# ==================================================================
# keras
# ------------------------------------------------------------------

RUN $PIP_INSTALL \
        h5py \
        keras;

# ==================================================================
# opencv
# ------------------------------------------------------------------

RUN DEBIAN_FRONTEND=noninteractive $APT_INSTALL \
        libatlas-base-dev \
        libgflags-dev \
        libgoogle-glog-dev \
        libhdf5-serial-dev \
        libleveldb-dev \
        liblmdb-dev \
        libprotobuf-dev \
        libsnappy-dev \
        protobuf-compiler; \
    $GIT_CLONE --branch 4.0.1 https://github.com/opencv/opencv ~/opencv; \
    mkdir -p ~/opencv/build && cd ~/opencv/build; \
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
          -D CMAKE_INSTALL_PREFIX=/usr/local \
          -D WITH_IPP=OFF \
          -D WITH_CUDA=OFF \
          -D WITH_OPENCL=OFF \
          -D BUILD_TESTS=OFF \
          -D BUILD_PERF_TESTS=OFF \
          ..; \
    make -j"$(nproc)" install; \
    ln -s /usr/local/include/opencv4/opencv2 /usr/local/include/opencv2;

# ==================================================================
# sonnet
# ------------------------------------------------------------------

RUN $PIP_INSTALL \
        tensorflow_probability \
        dm-sonnet;

# ==================================================================
# config & cleanup
# ------------------------------------------------------------------

RUN ldconfig; \
    apt-get clean; \
    apt-get autoremove; \
    rm -rf /var/lib/apt/lists/* /tmp/* ~/*;

EXPOSE 8888 6006