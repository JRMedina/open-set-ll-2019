import gc
import os
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from keras.callbacks import EarlyStopping, ModelCheckpoint
import keras.backend as K

from cnn_models import *
import data_handler


def train(feature_model, sim_model, x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val):
    # y_train = data_handler.force_one_to_two(y_train)
    # y_test = data_handler.force_one_to_two(y_test)

    # Optimizer settings
    # opt = Adam(lr=1e-6)

    # Categorical/accuracy/softmax - 67%
    # Binary/accuracy/softmax - 66%
    # kullback_leibler_divergence/accuracy/softmax - 66-68%
    # Cat/mse/sigmoid - 0.24
    # binary/mse/sigmoid - 0.22
    sim_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['mse', 'acc'])

    # Callbacks
    if not os.path.exists('saved_models/siamese_canonical/'):
        os.makedirs('saved_models/siamese_canonical/')
    filepath = "saved_models/siamese_canonical/weights-improvement-{epoch:02d}-{val_acc:.2f}.hdf5"
    checkpointing = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    early_stopping = EarlyStopping(monitor='val_loss', patience=20)
    best_model = ModelCheckpoint(filepath='saved_models/siamese_canonical/best_model.h5', monitor='val_loss', save_best_only=True)
    callbacks = [checkpointing, early_stopping, best_model]

    sim_model.fit(
        [x_train1, x_train2], y_train,
        batch_size=128,
        epochs=200,
        validation_data=([x_val1, x_val2], y_test),
        callbacks=callbacks
    )

    # Score best trained model checkpoint and return results
    sim_model.load_weights('saved_models/siamese_canonical/best_model.h5')
    scores = sim_model.evaluate([x_test1, x_test2], y_test, verbose=1)
    test_loss = scores[0]
    test_accuracy = scores[2]
    print('Test loss:', scores[0])
    print('Test MSE:', scores[1])
    print('Test accuracy:', scores[2])

    layer_name = 'out_2_a'
    intermediate_layer_model = Model(inputs=sim_model.input,
                                     outputs=sim_model.get_layer(layer_name).output)
    intermediate_output_a = intermediate_layer_model.predict(x=[x_test1, x_test2])
    print(intermediate_output)

    layer_name = 'out_2_b'
    intermediate_layer_model = Model(inputs=sim_model.input,
                                     outputs=sim_model.get_layer(layer_name).output)
    intermediate_output_b = intermediate_layer_model.predict(x=[x_test1, x_test2])
    print(intermediate_output)
    print(len(intermediate_output_a))
    print(len(x_test1))
    print(type(intermediate_output_b))

    return test_loss, test_accuracy, intermediate_output_a, intermediate_output_b


def run_through_metric_experiments():

    results1 = {'l1_distance': {'loss': [], 'accuracy': []}, 'euclidean': {'loss': [], 'accuracy': []}}

    # Get data
    (x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val) = data_handler.create_siamese_examples(classes_to_pick=[0, 1], number_of_pairs_per_class=5000)

    for distance_computation in ['l1_distance', 'euclidean']:
        for _ in range(0, 10):
            # Create and train model
            feature_model = cnn_canonical(x_train1, dense=64)
            similarity_model = sim_model_l1_one_pair(x_train1, feature_model, distance_computation=distance_computation,
                                                     add_2_layer=True)
            test_loss, test_accuracy = train(feature_model, similarity_model, x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val)

            results1[distance_computation]['loss'].append(test_loss)
            results1[distance_computation]['accuracy'].append(test_accuracy)

            # Clear up model
            K.clear_session()
            del(similarity_model)
            del(feature_model)
            K.clear_session()
            gc.collect()

    results2 = {'l1_distance': {'loss': [], 'accuracy': []}, 'euclidean': {'loss': [], 'accuracy': []}}

    for distance_computation in ['l1_distance', 'euclidean']:
        for _ in range(0, 10):
            # Create and train model
            feature_model = cnn_canonical(x_train1, dense=64)
            similarity_model = sim_model_l1_one_pair(x_train1, feature_model, distance_computation=distance_computation,
                                                     add_2_layer=False)
            test_loss, test_accuracy = train(feature_model, similarity_model, x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val)

            results2[distance_computation]['loss'].append(test_loss)
            results2[distance_computation]['accuracy'].append(test_accuracy)

            # Clear up model
            K.clear_session()
            del(similarity_model)
            del(feature_model)
            K.clear_session()
            gc.collect()

    print("2-layer")
    for distance_computation in ['l1_distance', 'euclidean']:
        print(distance_computation)
        print("test loss: {}".format(sum(results1[distance_computation]['loss'])/len(results1[distance_computation]['loss'])))
        print("test accuracy: {}".format(sum(results1[distance_computation]['accuracy'])/len(results1[distance_computation]['accuracy'])))

    print("no 2-layer")
    for distance_computation in ['l1_distance', 'euclidean']:
        print(distance_computation)
        print("test loss: {}".format(sum(results2[distance_computation]['loss'])/len(results2[distance_computation]['loss'])))
        print("test accuracy: {}".format(sum(results2[distance_computation]['accuracy'])/len(results2[distance_computation]['accuracy'])))


def capture_2_out():
    pass



def run_through_class_experiments():
    pass


if __name__ == '__main__':
    (x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val) = data_handler.create_siamese_examples(classes_to_pick=[0, 1], number_of_pairs_per_class=5000)

    feature_model = cnn_canonical(x_train1, dense=64, add_2_layer=False)
    similarity_model = sim_model_l1_one_pair(x_train1, feature_model, distance_computation='l1_distance', add_2_layer=True)

    train(feature_model, similarity_model, x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val)

    # run_through_metric_experiments()
