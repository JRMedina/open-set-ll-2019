"""This module is for creating, training, and testing KNN classifiers."""

import numpy as np
from sklearn.neighbors import KNeighborsClassifier

from open_set_comparison.data_loader import flatten_nd_array_to_2d


def knn_classifier(
    x_train: np.array,
    y_train: np.array,
    x_test: np.array,
    num_neighbors: int = 5,
    n_parallel_jobs: int = -1,
) -> np.ndarray:
    """Create, train, test, and return predictions from a KNN classifier.

    :param x_train: Input for creating/training the KNN classifier.
    :param y_train: Output for creating/training the KNN classifier.
    :param x_test: Input for testing the KNN classifier.
    :param num_neighbors: Number of neighbors comparisons for using to calculate the
    classification of a input.
    :param n_parallel_jobs: Number of parallel processes to use while testing the
    KNN classifier.
    :return: An array of the predictions based on the input from x_test.
    """
    # Flatten input if needed
    x_train = flatten_nd_array_to_2d(x_train)
    x_test = flatten_nd_array_to_2d(x_test)

    # Create and train classifier
    knn = KNeighborsClassifier(n_neighbors=num_neighbors, n_jobs=n_parallel_jobs)
    knn.fit(x_train, y_train)

    # Predict y_test from x_test
    y_pred = knn.predict(x_test)

    return y_pred
