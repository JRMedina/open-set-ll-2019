"""This module is used for loading, validating, and formatting data."""

from keras.datasets import mnist
import numpy as np
from typing import Tuple


def load_mnist_dataset() -> Tuple[np.array, np.array, np.array, np.array]:
    """Returns the MNIST database of handwritten digits. The MNIST dataset
    consists of 60,000 28x28 grayscale images of the 10 digits, along with
    a test set of 10,000 images.

    :return: Tuple of x and y-training, and x and y-testing np.arrays.
             x_train, x_test: uint8 array of grayscale image data with shape
                              (num_samples, 28, 28).
             y_train, y_test: uint8 array of digit labels (integers in range
                              0-9) with shape (num_samples,).
    """

    # Load MNIST dataset and return
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    return x_train, y_train, x_test, y_test


def flatten_nd_array_to_2d(array_to_flatten: np.ndarray) -> np.ndarray:
    """Flattens a ndim array where n is > 2, into a 2D np.ndarray. E.g. one
    case is flattening an array of images into an array of those images as
    1D vectors.

    :param array_to_flatten: Array to flatten.
    :return: array_to_flatten flattened into two dimensions.
    """

    # Check that the input is np.ndarray
    if type(array_to_flatten) != np.ndarray:
        raise ValueError(
            f"Expected np.ndarray, instead received {type(array_to_flatten)}."
        )

    # If ndim of array_to_flatten is less than 2, raise an error
    if array_to_flatten.ndim < 2:
        raise ValueError(
            f"Expected np.ndarray with dimensions of at least 2, instead received "
            f"np.ndarray wih ndim {array_to_flatten.ndim}."
        )

    # If received an array already in 2 dimensions, raise warning and return
    # original array
    if array_to_flatten.ndim == 2:
        return array_to_flatten

    flattened_array = []

    # Iterate through and flatten each item
    for array_item in array_to_flatten:
        flattened_array.append(array_item.flatten())

    return np.asarray(flattened_array)
