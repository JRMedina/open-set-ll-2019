"""This module is for creating, training, and testing SVM classifiers."""

import numpy as np
from sklearn.svm import SVC

from open_set_comparison.data_loader import flatten_nd_array_to_2d


def svm_classifier(
    x_train: np.array, y_train: np.array, x_test: np.array, kernel: str = "linear"
) -> np.ndarray:
    """Create, train, test, and return predictions from a SVM classifier.

    :param x_train: Input for creating/training the SVM classifier.
    :param y_train: Output for creating/training the SVM classifier.
    :param x_test: Input for testing the SVM classifier.
    :param kernel: The type of SVM kernel to use, defaults to "linear".
    :return: An array of the predictions based on the input from x_test.
    """
    # Flatten input if needed
    x_train = flatten_nd_array_to_2d(x_train)
    x_test = flatten_nd_array_to_2d(x_test)

    # Build and train a classifier
    svm = SVC(kernel=kernel)
    svm.fit(x_train, y_train)

    # Predict with SVM classifier
    y_predict = svm.predict(x_test)

    return y_predict
