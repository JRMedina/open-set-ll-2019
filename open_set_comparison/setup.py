#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

requirements = []

setup_requirements = []

test_requirements = ["pytest"]

setup(
    author="Julian Medina",
    author_email="julian.r.medina7@gmail.com",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    description="A project for comparing open-set learning in multiple algorithms and datasets.",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + "\n",
    include_package_data=True,
    keywords="open_set_comparison",
    name="open_set_comparison",
    packages=find_packages(include=["open_set_comparison"]),
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://git.eng.cosmicaes.com/JRMedina/open_set_comparison",
    version="0.1.0",
    package_data={"open_set_comparison": ["py.typed"]},
    zip_safe=False,
)
