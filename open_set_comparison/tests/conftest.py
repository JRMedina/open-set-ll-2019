"""File for creating pytest fixtures available to any pytest test function within this tests folder."""

import pytest

from open_set_comparison import open_set_comparison


@pytest.fixture
def response() -> None:
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    yield None
