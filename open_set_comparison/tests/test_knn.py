import numpy as np
import pytest


from open_set_comparison.data_loader import load_mnist_dataset, flatten_nd_array_to_2d
from open_set_comparison.knn import knn_classifier


def test_knn_classifier_3d_input() -> None:
    """Test that knn_classifier will take a 3D input, flatten it, and classify with it."""

    # Load mnist dataset
    x_train, y_train, x_test, y_test = load_mnist_dataset()

    # Build, train, and test knn classifier
    y_pred = knn_classifier(
        num_neighbors=1,
        x_train=x_train[:100],
        y_train=y_train[:100],
        x_test=x_test[:100],
    )

    # Check size and type of y_pred
    assert len(y_pred) == 100
    assert type(y_pred[0]) == np.uint8


def test_knn_classifier_2d_input() -> None:
    """Test that knn_classifier will take a 2D input and classify with it."""

    # Load mnist dataset
    x_train, y_train, x_test, y_test = load_mnist_dataset()

    x_train = x_train[:100]
    y_train = y_train[:100]
    x_test = x_test[:100]

    # Flatten to 2d for test
    x_train = flatten_nd_array_to_2d(x_train[:100])
    x_test = flatten_nd_array_to_2d(x_test[:100])

    # Build, train, and test knn classifier
    y_pred = knn_classifier(
        num_neighbors=1, x_train=x_train, y_train=y_train[:100], x_test=x_test
    )

    # Check size and type of y_pred
    assert len(y_pred) == 100
    assert type(y_pred[0]) == np.uint8
