import numpy as np
import pytest

from open_set_comparison.data_loader import load_mnist_dataset, flatten_nd_array_to_2d


def test_load_mnist_dataset() -> None:
    """Test the correct attributes of the mnist dataset returned from
    load_mnist_dataset."""

    # Load data
    x_train, y_train, x_test, y_test = load_mnist_dataset()

    # Check training data
    # Input
    assert len(x_train) == 60000  # 60,000 examples
    assert len(x_train[0]) == 28  # 28 rows of pixels
    assert len(x_train[0][0]) == 28  # 28 pixels per row
    # Output
    assert len(y_train) == 60000
    assert type(y_train[0]) == np.uint8  # One class per train example
    # Check only classes 0-9
    possible_classes = list(range(0, 10))
    for y_val in y_train:
        assert y_val in possible_classes

    # Check testing data
    # Input
    assert len(x_test) == 10000  # 10,000 test examples
    assert len(x_test[0]) == 28  # 28 rows of pixels
    assert len(x_test[0][0]) == 28  # 28 pixels per row
    # Ouput
    assert len(y_test) == 10000
    assert type(y_test[0]) == np.uint8  # One class per test example
    # Check only classes 0-9
    possible_classes = list(range(0, 10))
    for y_val in y_train:
        assert y_val in possible_classes


def test_flatten_nd_array_to_2d_single_value_error() -> None:
    """Test that if a single float value is passed to flatten_nd_array_to_2d it
    raises a ValueError.
    """

    with pytest.raises(ValueError) as err:
        flatten_nd_array_to_2d(1.0)

    assert str(err.value) == "Expected np.ndarray, instead received <class 'float'>."


def test_flatten_nd_array_to_2d_1d_array_error() -> None:
    """Test that if a 1D array is passed to flatten_nd_array_to_2d it raises a
    ValueError.
    """

    with pytest.raises(ValueError) as err:
        flatten_nd_array_to_2d(np.zeros(5))

    assert (
        str(err.value)
        == "Expected np.ndarray with dimensions of at least 2, instead received "
        "np.ndarray wih ndim 1."
    )


def test_flatten_nd_array_to_2d_2d_array_warning() -> None:
    """Test that if a 2D array is passed to flatten_nd_array_to_2d it returns the
    same array.
    """

    flattened_array = flatten_nd_array_to_2d(np.zeros((2, 5)))

    # Check the returned array is the same size and values
    np.testing.assert_array_equal(flattened_array, np.zeros((2, 5)))


def test_flatten_nd_array_to_2d_3d_array() -> None:
    """Test that if a 3D array is passed to flatten_nd_array_to_2d it returns the
    same array flattened to 2D.
    """

    flattened_array = flatten_nd_array_to_2d(np.zeros((2, 5, 5)))

    # Check the returned array is the same size and values
    np.testing.assert_array_equal(flattened_array, np.zeros((2, 25)))
