===================
open-set-comparison
===================

A project for comparing open-set learning in multiple algorithms and datasets.


* Free software: MIT license
* Documentation: https://open-set-comparison.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `JRMedina/python_cookie_cutter`_ project template.
