from keras.layers import Input, Dense, merge, Lambda, Embedding, Conv2D, GlobalMaxPooling2D, MaxPooling2D, Dropout,concatenate, Activation, BatchNormalization, Add
from keras.layers.core import Reshape, Flatten
from keras.models import Model
from keras import backend as K
import numpy as np


def similarity_model_one_pair(x_train, feature_model):
    img_a_in = Input(shape=x_train.shape[1:], name='ImageA_Input')
    img_b_in = Input(shape=x_train.shape[1:], name='ImageB_Input')
    img_a_feat = feature_model(img_a_in)
    img_b_feat = feature_model(img_b_in)
    combined_features = concatenate([img_a_feat, img_b_feat], name='merge_features')
    combined_features = Dense(2, activation = 'linear')(combined_features)
    combined_features = BatchNormalization()(combined_features)
    combined_features = Activation('relu')(combined_features)
    combined_features = Dense(1, activation='sigmoid')(combined_features)

    similarity_model = Model(inputs=[img_a_in, img_b_in],
                             outputs=[combined_features],
                             name='Similarity_Model')
    similarity_model.summary()

    return similarity_model

def sim_model_l1_one_pair(x_train, feature_model, distance_computation: str = 'l1_distance', add_2_layer: bool = False):
    img_a_in = Input(shape=x_train.shape[1:], name='ImageA_Input')
    img_b_in = Input(shape=x_train.shape[1:], name='ImageB_Input')
    img_a_feat = feature_model(img_a_in)
    img_b_feat = feature_model(img_b_in)
    # This is for later showing results on a 2d plane.
    if add_2_layer:
        img_a_feat = Dense(2, activation='linear')(img_a_feat)
        img_a_feat = BatchNormalization()(img_a_feat)
        img_a_feat= Activation('relu', name='out_2_a')(img_a_feat)

        img_b_feat = Dense(2, activation='linear')(img_b_feat)
        img_b_feat = BatchNormalization()(img_b_feat)
        img_b_feat = Activation('relu', name='out_2_b')(img_b_feat)

    # Compute distance
    if distance_computation == 'l1_distance':
        dist_compute = Lambda(lambda x: K.abs(x[0] - x[1]), name="distance")([img_a_feat,img_b_feat])
    elif distance_computation == 'euclidean': # 87%
        dist_compute = Lambda(lambda x: K.sqrt((x[0]-x[1])*(x[0]-x[1])), name="distance")([img_a_feat,img_b_feat])
    prediction = Dense(1, activation='sigmoid')(dist_compute)
    similarity_model = Model(inputs=[img_a_in, img_b_in],
                             outputs=[prediction],
                             name='Similarity_Model')
    # combined_features = concatenate([img_a_feat, img_b_feat], name='merge_features')
    # combined_features = Dense(2, activation = 'linear')(combined_features)
    # combined_features = BatchNormalization()(combined_features)
    # combined_features = Activation('relu')(combined_features)
    # combined_features = Dense(1, activation='sigmoid')(combined_features)

    # similarity_model = Model(inputs=[img_a_in, img_b_in],
    #                          outputs=[combined_features],
    #                          name='Similarity_Model')
    similarity_model.summary()

    return similarity_model


def cnn_kim(x_train, dense=2):
    filter_sizes = [2, 3, 5]
    num_filters = 100
    drop = 0.50

    # inputs = Input(shape=X_train[0].shape)
    inputs = Input(shape=x_train.shape[1:])

    conv_0 = Conv2D(num_filters, filter_sizes[0], activation='relu', padding="same")(inputs)
    conv_1 = Conv2D(num_filters, filter_sizes[1], activation='relu', padding="same")(inputs)
    conv_2 = Conv2D(num_filters, filter_sizes[2], activation='relu', padding="same")(inputs)

    maxpool_0 = MaxPooling2D(2)(conv_0)
    maxpool_1 = MaxPooling2D(2)(conv_1)
    maxpool_2 = MaxPooling2D(2)(conv_2)

    merged_tensor = concatenate([maxpool_0, maxpool_1, maxpool_2], axis=1)
    flatten = Flatten()(merged_tensor)
    dropout = Dropout(drop)(flatten)
    output = Dense(units=dense, activation='relu')(dropout)

    feature_model = Model(inputs, output, name='Feature_Model_CNN_Kim_{}'.format(dense))

    print(feature_model.summary())
    feature_model.save_weights('untrained_weights.h5')

    return feature_model


def cnn_res(x_train: np.ndarray, dense: int = 32, add_2_layer: bool = False):
    img_in = Input(shape=x_train.shape[1:], name='FeatureNet_ImageInput')
    n_layer = img_in
    for i in range(2):
        n_layer = Conv2D(3, kernel_size=(3, 3), activation='linear', padding='same')(n_layer)
        n_layer = BatchNormalization()(n_layer)
        n_layer = Activation('relu')(n_layer)
        res = Conv2D(3, kernel_size=1, padding='same')(n_layer)
        n_layer = Conv2D(3, kernel_size=(3, 3), activation='linear', padding='same')(n_layer)
        n_layer = BatchNormalization()(n_layer)
        n_layer = Activation('relu')(n_layer)
        n_layer = Add()([n_layer, res])
    n_layer = MaxPooling2D((2, 2))(n_layer)
    n_layer = Flatten()(n_layer)
    n_layer = Dense(dense, activation='linear')(n_layer)
    n_layer = Dropout(0.5)(n_layer)
    n_layer = BatchNormalization()(n_layer)
    n_layer = Activation('relu')(n_layer)

    # This is for later showing results on a 2d plane.
    if add_2_layer:
        n_layer = Dense(2, activation='linear')(n_layer)
        n_layer = Dropout(0.5)(n_layer)
        n_layer = BatchNormalization()(n_layer)
        n_layer = Activation('relu', name='out_2')(n_layer)

    feature_model = Model(img_in, n_layer, name='Feature_Model_CNN_Canonical_{}'.format(dense))

    print(feature_model.summary())
    feature_model.save_weights('untrained_weights_canonical.h5')

    return feature_model

def cnn_canonical(x_train: np.ndarray, dense: int = 32, add_2_layer: bool = True):
    img_in = Input(shape=x_train.shape[1:], name='FeatureNet_ImageInput')
    n_layer = img_in
    for i in range(2):
        n_layer = Conv2D(8 * 2 ** i, kernel_size=(3, 3), activation='linear')(n_layer)
        n_layer = BatchNormalization()(n_layer)
        n_layer = Activation('relu')(n_layer)
        n_layer = Conv2D(16 * 2 ** i, kernel_size=(3, 3), activation='linear')(n_layer)
        n_layer = BatchNormalization()(n_layer)
        n_layer = Activation('relu')(n_layer)
        n_layer = MaxPooling2D((2, 2))(n_layer)
    n_layer = Flatten()(n_layer)
    n_layer = Dense(dense, activation='linear')(n_layer)
    n_layer = Dropout(0.25)(n_layer)
    n_layer = BatchNormalization()(n_layer)
    n_layer = Activation('relu')(n_layer)

    # This is for later showing results on a 2d plane.
    if add_2_layer:
        two_layer = Dense(2, activation='linear', name='two_layer')(n_layer)
        # n_layer = Dropout(0.5)(two_layer)
        n_layer = BatchNormalization()(n_layer)
        n_layer = Activation('relu')(n_layer)

    feature_model = Model(img_in, n_layer, name='Feature_Model_CNN_Canonical_{}'.format(dense))

    print(feature_model.summary())
    feature_model.save_weights('untrained_weights_canonical.h5')

    return feature_model
