from keras.datasets import cifar10
import numpy as np
from tqdm import tqdm
import pickle
import math
import random
from datetime import datetime


random.seed(datetime.now())


def force_one_to_two(y_set: np.array):
    y_list= np.ndarray.tolist(y_set)
    y_final = []

    for y in tqdm(y_list):
        y_final.append([y, 0.0 if y == 1.0 else 1.0])

    return np.array(y_final)


def list_to_string(list_for_string: list) -> str:
    """Takes a list and returns a string with the elements separated by _

    :param list_for_string:
    :return:
    """
    return_string = ''

    for element in sorted(list_for_string):
        return_string += str(element) + '_'

    # Remove last '_' and return
    return return_string[:-1]


def shuffle_list(list_one_to_shuffle: list, list_two_to_shuffle: list) -> (list, list):
    """Shuffle two lists the same way maintaining relationship across.

    :param list_one_to_shuffle:
    :param list_two_to_shuffle:
    :return:
    """
    # Compile two lists and shuffle maintaining relationship across both lists.
    # I.e. element 1 list one and two are in the same shuffled position.
    list_to_shuffle = list(zip(list_one_to_shuffle, list_two_to_shuffle))
    random.shuffle(list_to_shuffle)

    # Take shuffled lists apart, and return
    one_shuffled, two_shuffled = zip(*list_to_shuffle)
    return (one_shuffled, two_shuffled)


def create_training_tuple(class_one_examples_list: list, class_two_examples_list: list):
    return (random.choice(class_one_examples_list), random.choice(class_two_examples_list))


def get_siamese_examples_for_class_from_classes(
        from_classes: list, num_of_pairs_per_class: int, class_dict: dict, type_data: str
) -> list:
    """

    Parameters
    ----------
    from_classes: list
        List of classes to pull examples from.
    num_of_pairs_per_class: int
        Number of pairs of examples per class that is being requested
    class_dict: dict
        Dictionary of all class data
    type_data: str
        Type of data either 'val', 'train, default is test

    Returns
    -------
    list
        A list of lists, each containing three elements, example a, b and, a np array 1.0
        or 0.0 for different or same class

    """

    examples = []

    for for_class in from_classes:
        num_examples = len(class_dict[for_class]['x'])
        eighty_percent = int(len(class_dict[for_class]['x']) * .80)
        rest_percent = len(class_dict[for_class]['x']) - eighty_percent

        # Get other classses available to select from
        classes_without = from_classes.copy()
        classes_without.remove(for_class)

        # For each class, get num_of_pairs_per_class examples
        for count in range(num_of_pairs_per_class):

            # First half of examples should be from same class
            if count < num_of_pairs_per_class/2:
                from_class = for_class
            # Randomly pick example class
            else:
                from_class = random.choice(classes_without)

            # Get training example from first 80% if train
            if type_data == 'train':
                rand_class_1, rand_class_2 = create_training_tuple(class_dict[for_class]['x'][:-rest_percent], class_dict[from_class]['x'][:-rest_percent])
            # Get example from last 20% if val
            elif type_data == 'val':
                rand_class_1, rand_class_2 = create_training_tuple(class_dict[for_class]['x'][num_examples - rest_percent:], class_dict[from_class]['x'][num_examples - rest_percent:])
            # Get example from anywhere in testing from testing dict
            else:
                rand_class_1, rand_class_2 = create_training_tuple(class_dict[for_class]['x'], class_dict[from_class]['x'])


            same = np.ones(1) if for_class == from_class else np.zeros(1)

            examples.append([rand_class_1, rand_class_2, same])

    return examples


def get_siamese_x_y(examples: list) -> (list, list):

    x1 = []
    x2 = []
    y = []

    for example in examples:
        x1.append(example[0])
        x2.append(example[1])
        y.append(example[2])

    return np.asarray(x1), np.asarray(x2), np.asarray(y)


def create_siamese_examples(classes_to_pick: list, number_of_pairs_per_class: int = 0):
    """Return siamese examples from the classes picked.

    :param classes_to_pick:
    :return:
    """

    number_of_classes = len(classes_to_pick)
    if number_of_classes <= 1:
        raise ValueError("One or less class passed in, you need at least two classes.")

    # Load required data dictionaries
    (train_class_dict, test_class_dict) = unpickle_me("train_test_cifar_tuple.pickle")

    num_class_examples = len(train_class_dict[0]['x'])
    # If no value passed in, due num examples / num classes
    if number_of_pairs_per_class == 0:
        number_of_pairs_per_class = int(math.floor(num_class_examples / number_of_classes))
    print("Doing {} examples per class...".format(number_of_pairs_per_class))

    train_examples = get_siamese_examples_for_class_from_classes(classes_to_pick, number_of_pairs_per_class,
                                                                 train_class_dict, type_data='train')

    val_examples = get_siamese_examples_for_class_from_classes(classes_to_pick, number_of_pairs_per_class,
                                                                 test_class_dict, type_data='val')

    test_examples = get_siamese_examples_for_class_from_classes(classes_to_pick, number_of_pairs_per_class,
                                                                 test_class_dict, type_data='test')

    x_train1, x_train2, y_train = get_siamese_x_y(train_examples)
    x_val1, x_val2, y_val = get_siamese_x_y(val_examples)
    x_test1, x_test2, y_test = get_siamese_x_y(test_examples)

    return (x_train1, x_train2, y_train, x_test1, x_test2, y_test, x_val1, x_val2, y_val)


def pick_these_classes(classes_to_pick: list):
    """Return sufficiently shuffled np.ndarray parallel sets for training and testing.

    :param classes_to_pick:
    :return: x_train, y_train,
    :return: x_train, y_train,
    :return: x_train, y_train,
    """

    # Load required data dictionaries
    (train_class_dict, test_class_dict) = unpickle_me("train_test_cifar_tuple.pickle")

    all_train_x = []
    all_test_x = []
    all_train_y = []
    all_test_y = []

    # Get requested training data and testing data
    for class_picked in classes_to_pick:
        all_train_x = all_train_x + train_class_dict[class_picked]['x']
        all_train_y = all_train_y + train_class_dict[class_picked]['y']
        all_test_x = all_test_x + test_class_dict[class_picked]['x']
        all_test_y = all_test_y + test_class_dict[class_picked]['y']

    # Shuffle data
    (all_train_x, all_train_y) = shuffle_list(all_train_x, all_train_y)
    (all_test_x, all_test_y) = shuffle_list(all_test_x, all_test_y)

    # Turn into np.ndarrays
    train_x = np.asarray(all_train_x)
    train_y = np.asarray(all_train_y)
    test_x = np.asarray(all_test_x)
    test_y = np.asarray(all_test_y)

    return (train_x, train_y), (test_x, test_y)


def pickle_me(directory_and_name: str, me) -> None:
    """Pickles a data structure.

    :param directory_and_name:
    :param me:
    :return:
    """

    with open(directory_and_name, 'wb') as pickle_file_handle:
        pickle.dump(me, pickle_file_handle, protocol=pickle.HIGHEST_PROTOCOL)


def unpickle_me(directory_and_name: str):
    """Unpickles a data structure.

    :param directory_and_name:
    :return:
    """

    with open(directory_and_name, 'rb') as pickle_file_handle:
        me = pickle.load(pickle_file_handle)

    return me


def add_example(x_example: np.ndarray, y_example: np.ndarray, class_dict: dict) -> None:
    """Add or instantiate y example in class dict.

    :param x_example:
    :param y_example:
    :param class_dict:
    :return: N/A since dicts are mutable in Python
    """

    y_val = y_example[0]

    # if y in class dict add x to existing example list for class
    if y_val in class_dict.keys():
        class_dict[y_val]['x'].append(x_example)
        class_dict[y_val]['y'].append(y_example)
    else:
        class_dict[y_val] = {}
        class_dict[y_val]['x'] = [x_example]
        class_dict[y_val]['y'] = [y_example]


def create_dict_with_classes_and_examples(x_examples: np.ndarray, y_examples: np.ndarray) -> dict:
    """Create dictionary separated by class containing a list of class examples.

    :param x_examples:
    :param y_examples:
    :return:
    """
    return_dict = {}

    for x, y in tqdm(zip(x_examples, y_examples), total=len(y_examples)):
        add_example(x, y, return_dict)

    return return_dict


def separate_cifar_10(pickle: bool = False) -> (dict, dict):
    """Split cifar-10 into its classes for testing and training.

    :param pickle:
    :param pickle_directory:
    :return:
    """

    (x_train, y_train), (x_test, y_test) = cifar10.load_data()

    train_dict = create_dict_with_classes_and_examples(x_train, y_train)
    test_dict = create_dict_with_classes_and_examples(x_test, y_test)

    if pickle:
        pickle_me("train_test_cifar_tuple.pickle", (train_dict, test_dict))

    return (train_dict, test_dict)


if __name__ == '__main__':
    separate_cifar_10(pickle=True)
