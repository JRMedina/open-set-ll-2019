import numpy as np

from data_handler import unpickle_me, pick_these_classes, shuffle_list, force_one_to_two,\
    list_to_string, create_training_tuple, get_siamese_examples_for_class_from_classes, \
    get_siamese_x_y


class TestDataHandler():

    def test_force_one_to_two(self):
        y_numpy_single_input = np.asarray([0.0, 1.0, 0.0, 1.0])

        out_array = list(force_one_to_two(y_set=y_numpy_single_input))
        print(out_array)

        # Check pairs of values came back and check each has 1.0 and a 0.0
        for y_pair in out_array:
            assert len(y_pair) == 2
            if y_pair[0] == 0.0:
                assert y_pair[1] == 1.0
            else:
                assert y_pair[1] == 0.0

    def test_list_to_string(self):

        test_list = [0, 1, 2, 3]

        test_string = list_to_string(test_list)

        assert  test_string == "0_1_2_3"

    def test_shuffle_list(self):
        list_a = [0, 1, 2, 3]
        list_b = [0, 1, 2, 3]

        (one_shuffled, two_shuffled) = shuffle_list(list_a, list_b)

        # Check each list is shuffled by the same amount and place, i.e. elements perform
        # same shuffle together
        for element_a, element_b in zip(one_shuffled, two_shuffled):
            assert element_a == element_b

    def test_create_training_tuple(self):

        list1 = [0, 1, 2, 3]
        list2 = [4, 5, 6, 7]

        # Check that random samples are generated and each one is from their proper set
        for _ in range(0, 10):
            ran1, ran2 = create_training_tuple(class_one_examples_list=list1, class_two_examples_list=list2)

            assert ran1 in list1
            assert ran2 in list2

    def test_separating_by_percent(self):
        test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

        num_examples = len(test_list)

        assert  num_examples == 10

        eighty_percent = int(len(test_list) * .80)

        assert  eighty_percent == 8

        rest_percent = len(test_list) - eighty_percent

        assert rest_percent == 2

        half_rest = int(rest_percent / 2)

        assert half_rest == 1

        # Check removing rest/20% gives first 80%, or 8
        assert test_list[:-rest_percent] == [0, 1, 2, 3, 4, 5, 6, 7]

        assert test_list[num_examples - rest_percent:] == [8, 9]


    def test_get_siamese_examples_for_class_from_classes(self):

        (train_dict, test_dict) = unpickle_me("train_test_cifar_tuple.pickle")

        from_classes = [0, 1]
        num_of_pairs_per_class = 10
        class_dict = train_dict
        type_data = "train"

        siamese_examples = get_siamese_examples_for_class_from_classes(
            from_classes=from_classes,
            num_of_pairs_per_class=num_of_pairs_per_class,
            class_dict=train_dict,
            type_data=type_data
        )

        eighty_percent = int(len(train_dict[0]['x']) * .80)
        rest_percent = len(train_dict[0]['x']) - eighty_percent

        assert len(siamese_examples) == 20

        for example_list in siamese_examples:
            assert len(example_list) == 3

    def test_get_siamese_x_y(self):
        (train_dict, test_dict) = unpickle_me("train_test_cifar_tuple.pickle")

        from_classes = [0, 1]
        num_of_pairs_per_class = 10

        siamese_examples = get_siamese_examples_for_class_from_classes(
            from_classes=from_classes,
            num_of_pairs_per_class=num_of_pairs_per_class,
            class_dict=train_dict,
            type_data="train"
        )

        x1_array, x2_array, y_array = get_siamese_x_y(examples=siamese_examples)

        assert len(x1_array) == len(x2_array)

        assert len(x2_array) == len(y_array)

        one_cnt = 0
        zero_cnt = 0

        for y_val in y_array:
            if y_val.item() == 1.0:
                one_cnt += 1
            else:
                zero_cnt += 1

        assert one_cnt == zero_cnt

        siamese_examples = get_siamese_examples_for_class_from_classes(
            from_classes=from_classes,
            num_of_pairs_per_class=num_of_pairs_per_class,
            class_dict=train_dict,
            type_data="val"
        )

        x1_array, x2_array, y_array = get_siamese_x_y(examples=siamese_examples)

        assert len(x1_array) == len(x2_array)

        assert len(x2_array) == len(y_array)

        one_cnt = 0
        zero_cnt = 0

        for y_val in y_array:
            if y_val.item() == 1.0:
                one_cnt += 1
            else:
                zero_cnt += 1

        assert one_cnt == zero_cnt

        siamese_examples = get_siamese_examples_for_class_from_classes(
            from_classes=from_classes,
            num_of_pairs_per_class=num_of_pairs_per_class,
            class_dict=test_dict,
            type_data="test"
        )

        x1_array, x2_array, y_array = get_siamese_x_y(examples=siamese_examples)

        assert len(x1_array) == len(x2_array)

        assert len(x2_array) == len(y_array)

        one_cnt = 0
        zero_cnt = 0

        for y_val in y_array:
            if y_val.item() == 1.0:
                one_cnt += 1
            else:
                zero_cnt += 1

        assert one_cnt == zero_cnt

        from_classes = [0, 1, 2]
        num_of_pairs_per_class = 10

        siamese_examples = get_siamese_examples_for_class_from_classes(
            from_classes=from_classes,
            num_of_pairs_per_class=num_of_pairs_per_class,
            class_dict=train_dict,
            type_data="train"
        )

        x1_array, x2_array, y_array = get_siamese_x_y(examples=siamese_examples)

        assert len(x1_array) == len(x2_array)

        assert len(x2_array) == len(y_array)

        one_cnt = 0
        zero_cnt = 0

        for y_val in y_array:
            if y_val.item() == 1.0:
                one_cnt += 1
            else:
                zero_cnt += 1

        assert one_cnt == zero_cnt

    def test_pick_classes(self):
        """Test picking specific classes for training and testing is correct in sizing and random.

        Complete these for both train and test data
        1. Check all classes exist in class dict keys
        2. Check each as an item dict with keys 'x' and 'y'
        3. These dicts are equal in length which are equal to each other classes'
        """
        (train_class_dict, test_class_dict) = unpickle_me("train_test_cifar_tuple.pickle")
        (train_x, train_y), (test_x, test_y) = pick_these_classes([0, 1])

        assert len(train_x) == 10000
        assert len(train_y) == 10000
        assert len(test_x) == 2000
        assert len(test_y) == 2000

    def test_class_dict_correct_format_cifar(self):
        """Test pickled cifar class dict for train and testing for correct format.

        Complete these for both train and test data
        1. Check all classes exist in class dict keys
        2. Check each as an item dict with keys 'x' and 'y'
        3. These dicts are equal in length which are equal to each other classes'
        """

        # Unpickle cifar into train and test dict
        correct_classes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        (train_dict, test_dict) = unpickle_me("train_test_cifar_tuple.pickle")

        # 1. Check all classes exist in class dict keys
        assert list(train_dict.keys()) == correct_classes
        assert list(test_dict.keys()) == correct_classes

        # 2. Check each as an item dict with keys 'x' and 'y'
        for testing_dict in [train_dict, test_dict]:
            for class_num, class_example_dict in testing_dict.items():
                assert 'x' in class_example_dict.keys()
                assert 'y' in class_example_dict.keys()

        # 3. These dicts are equal in length which are equal to each other classes'
        for testing_dict in [train_dict, test_dict]:
            true_length = len(testing_dict[0]['x'])

            for class_num, class_example_dict in testing_dict.items():
                x_list = class_example_dict['x']
                y_list = class_example_dict['y']

                assert true_length == len(x_list)
                assert len(x_list) == len(y_list)
